/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.IvaAlfiyanti.sewavila.model;

/**
 *
 * @author Fujitsu
 */
public class Info {
    private final String aplikasi = "Aplikasi Sewa Villa";
    private final String greeting = "=====Selamat Datang=====";

    public String getAplikasi() {
        return aplikasi;
    }

    public String getGreeting() {
        return greeting;
    }
}
