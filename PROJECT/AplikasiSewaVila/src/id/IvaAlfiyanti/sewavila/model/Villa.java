/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.IvaAlfiyanti.sewavila.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 *
 * @author Fujitsu
 */
public class Villa  implements Serializable {
    private static final long serialVersionUID = -6756463875294313469L;

    private String namapenyewa;
    private String nomorhp;
    private int lamasewa;
    private int Fasilitas;
    private LocalDateTime hariMasuk;
    private LocalDateTime waktuKeluar;
    private BigDecimal biaya;
    private boolean keluar = false;

    public Villa() {

    }

    public Villa(String namapenyewa, String nomorhp, int lamasewa, int Fasilitas, LocalDateTime hariMasuk, LocalDateTime waktuKeluar, BigDecimal biaya) {
        this.namapenyewa = namapenyewa;
        this.nomorhp = nomorhp;
        this.lamasewa = lamasewa;
        this.Fasilitas = Fasilitas;
        this.hariMasuk = hariMasuk;
        this.waktuKeluar = waktuKeluar;
        this.biaya = biaya;
    }

    public String getNamapenyewa() {
        return namapenyewa;
    }

    public void setNamapenyewa(String namapenyewa) {
        this.namapenyewa = namapenyewa;
    }

    public String getNomorhp() {
        return nomorhp;
    }

    public void setNomorhp(String nomorhp) {
        this.nomorhp = nomorhp;
    }

    public int getLamasewa() {
        return lamasewa;
    }

    public void setLamasewa(int lamasewa) {
        this.lamasewa = lamasewa;
    }

    public int getFasilitas() {
        return Fasilitas;
    }

    public void setFasilitas(int Fasilitas) {
        this.Fasilitas = Fasilitas;
    }

    public LocalDateTime getHariMasuk() {
        return hariMasuk;
    }

    public void setHariMasuk(LocalDateTime hariMasuk) {
        this.hariMasuk = hariMasuk;
    }

    public LocalDateTime getWaktuKeluar() {
        return waktuKeluar;
    }

    public void setWaktuKeluar(LocalDateTime waktuKeluar) {
        this.waktuKeluar = waktuKeluar;
    }

    public BigDecimal getBiaya() {
        return biaya;
    }

    public void setBiaya(BigDecimal biaya) {
        this.biaya = biaya;
    }

    public boolean isKeluar() {
        return keluar;
    }

    public void setKeluar(boolean keluar) {
        this.keluar = keluar;
    }


    
    @Override
    public String toString() {
        return "Villa{" + " nama Penyewa = " + namapenyewa + ", nomor Hp = " + nomorhp + ", Lama Sewa =" + lamasewa + ", Fasilitas = " + Fasilitas + ", Hari Masuk = " + hariMasuk + ", Waktu keluar= " + waktuKeluar + "Biaya = " + biaya + ", keluar = " + keluar + '}';
    }
}