/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.IvaAlfiyanti.sewavila.controller;

import id.IvaAlfiyanti.sewavila.model.Info;
import java.util.Scanner;

/**
 *
 * @author Fujitsu
 */
public class Menu {
    private final Scanner in = new Scanner(System.in);
    private int noMenu;

    public void getMenuAwal() {
        Info info = new Info();

        System.out.println("=================================================");
        System.out.println(info.getAplikasi());
        System.out.println(info.getGreeting());
        System.out.println("-------------------------------------------------");

        System.out.println("DAFTAR MENU");
        System.out.println("1. Menu Villa");
        System.out.println("2. Menu Checkout Villa");
        System.out.println("3. Laporan Mingguan ");
        System.out.println("4. Keluar Aplikasi");
        System.out.println("=================================================");
        System.out.print("Pilih Menu (1/2/3/4) : ");

        do {
            while (!in.hasNextInt()) {
                String input = in.next();
                System.out.printf("\"%s\" is not a valid number.\n", input);
                System.out.print("Pilih Menu (1/2/3/4) : ");
            }
            noMenu = in.nextInt();
        } while (noMenu < 0);
        setPilihMenu();
    }

    public void setPilihMenu() {
        VillaController vc = new VillaController();
        switch (noMenu) {
            case 1:
                vc.setMasukVilla();
                break;
            case 2:
                vc.setCheckoutVilla();
                break;
            case 3:
                vc.getDataVilla();
                break;
            case 4:
                System.out.println("=== Sampai jumpa ===");
                System.exit(0);
                break;
        }
    }
}
