/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.IvaAlfiyanti.sewavila.controller;

import com.google.gson.Gson;
import id.IvaAlfiyanti.sewavila.model.Villa;
import java.io.FileNotFoundException;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Scanner;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 *
 * @author Fujitsu
 */
public class VillaController {

    private static final String FILE = "D://villa.json";
    private Villa villa;
    private final Scanner in;
    private String namapenyewa;
    private String nomorhp ;
    private int lamasewa;
    private int Fasilitas;
    private LocalDateTime hariMasuk;
    private LocalDateTime waktuKeluar;
    private BigDecimal biaya;
    private final DateTimeFormatter dateTimeFormat;
    private int pilihan;
    private boolean keluar = false;

    public VillaController() {
        in = new Scanner(System.in);
        hariMasuk = LocalDateTime.now();
        waktuKeluar = LocalDateTime.now();
        dateTimeFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    }

    public void setMasukVilla() {
       
        System.out.println("Masukkan Nama penyewa : ");
        namapenyewa = in.next();
        System.out.println(" Masukkan no HP : ");
        nomorhp = in.next();
        String formathariMasuk = hariMasuk.format(dateTimeFormat);
        System.out.println("Waktu Masuk : " + formathariMasuk);
        
        System.out.println("Lama Sewa : ");
        lamasewa = in.nextInt();
        
        System.out.println("Jenis Rumah & fasilitas ");
        System.out.println(" 1. Rumah + fasilitas biasa, 2.Rumah + fasilitas lengkap , 3. Rumah + fasilitas super lengkap ");
        Fasilitas = in.nextInt();
        if (lamasewa > 0) {
                biaya = new BigDecimal(lamasewa);
                if (Fasilitas == 1) {
                    biaya = biaya.multiply(new BigDecimal(1500000));
                } else if (Fasilitas == 2) {
                    biaya = biaya.multiply(new BigDecimal(2000000));
                } else if (Fasilitas == 3) {
                     biaya = biaya.multiply(new BigDecimal(2500000));
                }
            } 
        villa = new Villa();
        
        villa.setNamapenyewa(namapenyewa);
        villa.setNomorhp(nomorhp);
        villa.setLamasewa(lamasewa);
        villa.setFasilitas(Fasilitas);
        villa.setHariMasuk(hariMasuk);
        villa.setBiaya(biaya);
        setWriteVilla(FILE, villa);
        
        
        System.out.println("Apakah mau Input kembali?");
        System.out.print("1) Ya, 2) Tidak : ");
        pilihan = in.nextInt();
        if (pilihan == 2) {
            Menu m = new Menu();
            m.getMenuAwal();
        } else {
            setMasukVilla();
        }
    }

    public void setWriteVilla(String file, Villa villa) {
        Gson gson = new Gson();

        List<Villa>villas = getReadVilla(file);
        villas.remove(villa);
        villas.add(villa);

        String json = gson.toJson(villas);
        try {
            FileWriter writer = new FileWriter(file);
            writer.write(json);
            writer.close();
        } catch (IOException ex) {
            Logger.getLogger(VillaController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void setCheckoutVilla() {
        System.out.print("Nama Penyewa : ");
        namapenyewa = in.next();

        Villa v = getSearch(namapenyewa);
        
        if (v != null) {
          
            waktuKeluar = LocalDateTime.now();
            v.setWaktuKeluar(waktuKeluar);

            System.out.println("Nama Penyewa : " + v.getNamapenyewa());
            System.out.println("No HP : " + v.getNomorhp());
            System.out.print("No Rumah & Fasilitas : ");
            if (v.getFasilitas() == 1) {
                System.out.println("Rumah + fasilitas biasa");
            } else if (v.getFasilitas() == 2) {
                System.out.println("Rumah + failitas lengkap");
            } else if (v.getFasilitas() == 3) {
                System.out.println("Rumah + fasilitas super lengkap");
            }
            hariMasuk = LocalDateTime.now();
            v.setHariMasuk(hariMasuk);
            
            System.out.println("Waktu Masuk : " + v.getHariMasuk().format(dateTimeFormat));
            System.out.println("Waktu Keluar : " + v.getWaktuKeluar().format(dateTimeFormat));
            System.out.println("Lama : " + v.getLamasewa() + " Hari ");
            System.out.println("Biaya : " + v.getBiaya());
            
            
            
            System.out.println("Proses Bayar?");
            System.out.print("1) Ya, 2) Tidak, 3) Keluar : ");
            pilihan = in.nextInt();
            switch (pilihan) {
                case 1:
                    v.setKeluar(true);
                    setWriteVilla(FILE, v);
                    break;
                case 2:
                    setCheckoutVilla();
                    break;
                default:
                    Menu m = new Menu();
                    m.getMenuAwal();
                    break;
            }

            System.out.println("Apakah mau memproses kembali?");
            System.out.print("1) Ya, 2) Tidak : ");
            pilihan = in.nextInt();
            if (pilihan == 2) {
                Menu m = new Menu();
                m.getMenuAwal();
            } else {
                setCheckoutVilla();
            }
        } else {
            System.out.println("Data tidak ditemukan");
            setCheckoutVilla();
        }
    }

    public Villa getSearch(String namapenyewa) {
        List<Villa> villas = getReadVilla(FILE);// Arrays.asList(villa);

        Villa v = villas.stream()
                .filter(pp -> namapenyewa.equalsIgnoreCase(pp.getNamapenyewa()))
                .findAny()
                .orElse(null);

        return v;
    }

    public List<Villa> getReadVilla(String file) {
        List<Villa> villas = new ArrayList<>();

        Gson gson = new Gson();
        String line = null;
        try ( Reader reader = new FileReader(file)) {
            BufferedReader br = new BufferedReader(reader);
            while ((line = br.readLine()) != null) {
                Villa[] vs = gson.fromJson(line, Villa[].class);
                villas.addAll(Arrays.asList(vs));
            }
            br.close();
            reader.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(VillaController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(VillaController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return villas;
    }
   public void getDataVilla() {
        List<Villa> villas = getReadVilla(FILE);
        Predicate<Villa> isKeluar = e -> e.isKeluar() == true;
        Predicate<Villa> isDate = e -> e.getHariMasuk().toLocalDate().equals(LocalDate.now());

        List<Villa> vResults = villas.stream().filter(isKeluar.and(isDate)).collect(Collectors.toList());
        BigDecimal total = vResults.stream()
                .map(Villa::getBiaya)
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        System.out.println("Fasilitas \t\tNama Penyewa \t\tNo HP \t\tWaktu masuk Villa \t\tPembayaran Villa \t\tWaktu Keluar \t\tBiaya");
        System.out.println("--------- \t\t-----------  \t\t-------\t\t----------------- \t\t---------------- \t\t------------ \t\t----");
        vResults.forEach((v) -> {
            System.out.println(v.getFasilitas() + "\t\t\t" + v.getNamapenyewa() + "\t\t\t" + v.getNomorhp()+ "\t\t" + v.getHariMasuk().format(dateTimeFormat) + "\t\t" + v.getLamasewa()+ "\t\t" + v.getWaktuKeluar().format(dateTimeFormat) + "\t\t" + v.getBiaya());
        });
        System.out.println("--------- \t\t-----------  \t\t-------\t\t----------------- \t\t---------------- \t\t------------ \t\t----");
        System.out.println("====================================");
        System.out.println("Pendapatan Total = Rp. " + total);
        System.out.println("====================================");

        System.out.println("Apakah mau mengulanginya?");
        System.out.print("1) Ya, 2) Tidak : ");
        pilihan = in.nextInt();
        if (pilihan == 2) {
            Menu m = new Menu();
            m.getMenuAwal();
        } else {
            getDataVilla();
        }
    
                }
}
