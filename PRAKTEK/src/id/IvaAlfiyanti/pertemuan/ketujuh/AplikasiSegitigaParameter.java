/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.IvaAlfiyanti.pertemuan.ketujuh;
import java.util.Scanner;
/**
 *
 * @author Fujitsu
 */
public class AplikasiSegitigaParameter {
    public static void main(String[] args) {
        int i, N;
        float a, t;
        Scanner in = new Scanner(System.in);
        
        System.out.println("Tentukan banyaknya Segitiga yang ingin di Hitung: ");
        N = in.nextInt();
        
        for (i = 1; i <= N; i++) {
            System.out.println("Masukkan nilai alas : ");
            a = in.nextFloat();
            System.out.println("Masukkan nilai tinggi : ");
            t = in.nextFloat();
            SegitigaParameter segitigaParameter = new SegitigaParameter(a,t);
        }
    }
}
