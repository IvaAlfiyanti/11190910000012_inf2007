/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.IvaAlfiyanti.pertemuan.ketujuh;
import java.util.Scanner;
/**
 *
 * @author Fujitsu
 */
public class Segitiga {
    double alas;
    double tinggi;
    double luas;
    Scanner in = new Scanner(System.in);
    
    public Segitiga() {
        System.out.println("===Luas Segitiga===");
        System.out.println("Masukkan alas: ");
        alas = in.nextDouble();
        System.out.println("Masukkan tinggi: ");
        tinggi = in.nextDouble();
        luas = (alas * tinggi) / 2;
        System.out.println("Luas : " + luas);
    }
}
        