/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.IvaAlfiyanti.pertemuan.ketujuh;

/**
 *
 * @author Fujitsu
 */
public class AplikasiFungsi {
    public static void main(String[] args) {
        float x;
        Fungsi hasilFungsi = new Fungsi();
        System.out.println("-------------");
        System.out.println("  x    f(x)  ");
        System.out.println("-------------");
        
        x = (float) 10.0;
        while (x <= 15.0) {
            System.out.println(x + " " + hasilFungsi.getHasil(x));
            x = (float) (x + 0.2);  
        }
        System.out.println("--------------");
    } 
}
