/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.IvaAlfiyanti.pertemuan.keenam;
import java.util.Scanner;
/**
 *
 * @author Fujitsu
 */
public class HandlingThrow {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        try {
            System.out.println("Masukkan Angka: "); 
            int num = in.nextInt();
            if (num>10) throw new Exception();
            System.out.println("Angka kurang dari atau sama dengan 10");
        } catch (Exception err) {
            System.out.println("Angka lebih dari 10");
        }
        System.out.println("Selesai");   
    }
}
