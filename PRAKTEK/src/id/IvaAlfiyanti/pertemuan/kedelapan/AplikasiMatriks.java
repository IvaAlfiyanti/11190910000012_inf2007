package id.IvaAlfiyanti.pertemuan.kedelapan;
import java.util.Scanner;

public class AplikasiMatriks {

    public static void main(String[] args) {
        int i, j, Bar, Kol;
        Matriks matriks = new Matriks();
        Scanner in = new Scanner(System.in);
        System.out.println("Masukkan jumlah Baris : ");
        Bar = in.nextInt();
        System.out.println("Masukkan jumlah Kolom : ");
        Kol = in.nextInt();
        int[][] Array1 = new int[Bar][Kol];
        int[][] Array2 = new int[Bar][Kol];

        for (i = 0; i < Bar; i++) {
            System.out.println("Masukkan nilai Larik 1 Baris ke " + (i + 1));
            for (j = 0; j < Kol; j++) {
                Array1[i][j] = in.nextInt();
            }
            for (i = 0; i < Bar; i++) {
                System.out.println("Masukkan nilai Larik 2 Baris ke " + (i + 1));
                for (j = 0; j < Kol; j++) {
                    Array2[i][j] = in.nextInt();
                }
            }
            System.out.println("Matriks ke 1");
            for (i = 0; i < Bar; i++) {
                System.out.println("[");
                for (j = 0; j < Kol; j++) {
                    System.out.println(" " + Array1[i][j] + " ");
                }
                System.out.println("]");
            }
            System.out.println("Matriks ke 2");
            for (i = 0; i < Bar; i++) {
                System.out.println("[");
                for (j = 0; j < Kol; j++) {
                    System.out.println(" " + Array2[i][j] + " ");
                }
                System.out.println("]");
            }
            System.out.println("Matriks hasil penjumlahan = ");
            matriks.getPenambahanMatriks(Array1, Array2, Bar, Kol);

        }
    }
}
