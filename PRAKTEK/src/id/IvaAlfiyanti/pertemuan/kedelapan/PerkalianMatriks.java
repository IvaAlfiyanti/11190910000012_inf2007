/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.IvaAlfiyanti.pertemuan.kedelapan;
import java.util.Scanner;
/**
 *
 * @author Fujitsu
 */
public class PerkalianMatriks {
    public static int cetak(int[][] matriks) {
        for (int i = 0; i < 3; i++) {
            System.out.println("[");
            for (int j = 0; j < 5; j++) {
                System.out.println(" " + matriks[i][j] + " ");
               
            }
            System.out.println("]");
        } 
        
        return 0;
    }
    
    public static int kali(int[][] matriks) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 5; j++) {
                matriks[i][j] = matriks[i][j] * 3;
            }
        }
        return 0;
    }
    public static void main(String[] args) {
        int matriks [][] = new int [3][5];
        Scanner in = new Scanner(System.in);
        for (int i = 0; i < 3; i++) {
            System.out.println("Masukkan nilai Larik Baris ke " + (i + 1));
            for (int j = 0; j < 5; j++) {
                matriks[i][j] = in.nextInt();
            }
        }
        System.out.println("Matriks 3 x 5 : ");
        cetak(matriks);
        kali(matriks);
        System.out.println(" Matriks 3 x 5 setelah dikali 3 : ");
        cetak(matriks);
    }
}
    
