/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.IvaAlfiyanti.pertemuan.kedelapan;
import java.util.Scanner;
/**
 *
 * @author Fujitsu
 */
public class ElemenTerkecil {
    public static int getMin(int A[], int n) {
        int i, min = 9999;
        for (i = 0; i < n; i++) {
            if (A[i] < min) {
                min = A[i];
            }
        }return min;
    }
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n, i;
        System.out.println("Jumlah Elemen Larik : ");
        n = in.nextInt();
        int A[] = new int[n];
        ArrayMaksimum Maksimum = new ArrayMaksimum();
        
        System.out.println("Masukkan nilai Elemen Larik : ");
        for (i = 0; i < n; i++) {
            A[i] = in.nextInt();
        }
        System.out.println("Elemen terkecil : " + getMin(A, n));
    }
}
