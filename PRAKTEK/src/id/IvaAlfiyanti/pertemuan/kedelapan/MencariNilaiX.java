
package id.IvaAlfiyanti.pertemuan.kedelapan;
import java.util.Scanner;
/**
 *
 * @author Fujitsu
 */
public class MencariNilaiX {
    public static int MencariX(int angka[], int X) {
        int i;
        for (i = 0; i < angka.length; i++) {
            if (angka[i] == X) {
                return i + 1;
            }
        }
        return 0;
    }
    
    public static void main(String[] args) {
        int i, X, n;
        Scanner in = new Scanner(System.in);
        System.out.println("Banyaknya Elemen yang ingin di Masukkan : ");
        n = in.nextInt();
        int angka[] = new int[n];
        System.out.println("Masukkan Elemen = ");
        
        for (i = 0; i < n; i++) {
            angka[i] = in.nextInt();
        }
        System.out.println("Masukkan nilai yang ingin dicari : ");
        X = in.nextInt();
        int hasil = MencariX (angka , X);
        if (hasil == 0) {
            System.out.println("Indeks 0");
        }else {
            System.out.println("Urutan ke " + MencariX(angka, X));
        }
    }
}