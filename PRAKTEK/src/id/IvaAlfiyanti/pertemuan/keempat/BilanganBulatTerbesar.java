/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.IvaAlfiyanti.pertemuan.keempat;
import java.util.Scanner;
/**
 *
 * @author Fujitsu
 */
public class BilanganBulatTerbesar {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int numeral1 = in.nextInt();
        int numeral2 = in.nextInt();
        int numeral3 = in.nextInt();
        int maks = 0;
        
        if (numeral1 > numeral2) {
            maks = numeral1;
        }else if (numeral2 >= numeral1) {
            maks = numeral2;
        }
        if (numeral3 > maks) {
            maks = numeral3;
        }
        System.out.println("Bilangan bulat terbesar = " + maks);
    }
}
