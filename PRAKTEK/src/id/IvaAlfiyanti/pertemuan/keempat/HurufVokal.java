/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.IvaAlfiyanti.pertemuan.keempat;
import java.util.Scanner;

/**
 *
 * @author Fujitsu
 */
public class HurufVokal {
    public static void main(String[] args) {
        char Huruf;
        Scanner input = new Scanner(System.in);
        Huruf = input.next().charAt(0);
        
        if (Huruf == 'a' || Huruf == 'i' || Huruf == 'u' || Huruf == 'e' || Huruf == 'o') {
            System.out.println("Huruf Vokal");
        }            
    }
}
