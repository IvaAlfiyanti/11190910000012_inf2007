/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.IvaAlfiyanti.pertemuan.kelima;
import java.util.Scanner;

public class MenuPersegiPanjang {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        
        int noMenu;
        float panjang, lebar;
        float luas, keliling, diagonal;
        
        do {
            System.out.println("Menu Empat Persegi Panjang");
            System.out.println("1. Hitung Luas");
            System.out.println("2. Hitung Keliling");
            System.out.println("3. Hitung Panjang Diagonal");
            System.out.println("4. Keluar Program");
            System.out.println("Masukkan pilihan anda (1/2/3/4) ?");
            
            noMenu = in.nextInt();
            switch (noMenu) {
                case 1: {
                    panjang = in.nextFloat();
                    lebar = in.nextFloat();
                    luas = panjang * lebar;
                    System.out.println("Luas");
                }
                    break;
                
                case 2: {
                    panjang = in.nextFloat();
                    lebar = in.nextFloat();
                    keliling = 2 * panjang + 2 * lebar;
                    System.out.println("Keliling");
                }
                    break;
                    
                case 3 : {
                    panjang = in.nextFloat();
                    lebar = in.nextFloat();
                    diagonal = (panjang * panjang + lebar * lebar);
                    System.out.println("Diagonal");
                }
                    break;
                    
                case 4 : {
                    System.out.println("Keluar program ... sampai jumpa");
                }
            }        
              }while (noMenu != 4);
        }
        }
        
