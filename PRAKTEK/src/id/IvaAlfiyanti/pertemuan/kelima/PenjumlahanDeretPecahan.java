/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.IvaAlfiyanti.pertemuan.kelima;
import java.util.Scanner;

public class PenjumlahanDeretPecahan {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        
        int x;
        float s;
        
        s = 0;
        x = in.nextInt();
        
        while (x != -1) {
            s = s + (float)1 / x;
            x = in.nextInt();
        }
        System.out.println(s);           
        }
    }
