/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.IvaAlfiyanti.pertemuan.kelima;
import java.util.Scanner;
/**
 *
 * @author Fujitsu
 */
public class Faktorial {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        
        int n;
        int fak;
        int i;
        
        n = in.nextInt();
        fak = 1;
        for (i = 1; i <= n; i++) {
            fak = fak * i;  
        }
        System.out.println(fak);
    }
}
