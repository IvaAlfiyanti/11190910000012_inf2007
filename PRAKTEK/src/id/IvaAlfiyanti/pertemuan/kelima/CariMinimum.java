/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.IvaAlfiyanti.pertemuan.kelima;
import java.util.Scanner;
/**
 *
 * @author Fujitsu
 */
public class CariMinimum {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        
        int N;
        int x;
        int min;
        int i;
        
        N = in.nextInt();
        x = in.nextInt();
        min = x;
        System.out.println("nilai yang akan ditandingkan ada " + N);
        
        for (i = 2; i <= N; i++) {
            x = in.nextInt();
            if ( x < min ) {
                min = x;
            }
        }
        System.out.println(min + "adalah nilai paling minimum");
    }
}
