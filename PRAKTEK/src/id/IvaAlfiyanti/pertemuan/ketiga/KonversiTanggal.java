/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.IvaAlfiyanti.pertemuan.ketiga;
import java.util.Scanner;
/**
 *
 * @author Fujitsu
 */
public class KonversiTanggal {
    public static void main(String[] args) {
        int jarakY, kilometer, meter, sentimeter, sisaJarak, satuMeter = 100, satuKilometer = 100000;
        
        Scanner jarak = new Scanner(System.in);
        System.out.println("jarak (cm) : ");
        jarakY = jarak.nextInt();
        
        kilometer = jarakY / satuKilometer;
        sisaJarak = jarakY % satuKilometer;
        meter = sisaJarak / satuMeter;
        sisaJarak = sisaJarak % satuMeter;
        sentimeter = sisaJarak;
        
        System.out.println("jarak yang ditemput adalah " + kilometer + " km " + meter + " m " + sentimeter + " cm ");
    }
}
