/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.IvaAlfiyanti.pertemuan.ketiga;
import java.util.Scanner;
/**
 *
 * @author Fujitsu
 */
public class SelisihDuaTanggal {
    public static void main(String[] args) {
        int tgl1, bulan1, thn1, tgl2, bulan2, thn2, tgl3, bulan3, thn3, selisih,JumlahHari1, JumlahHari2, JumlahHari3, Sisa;
        Scanner in = new Scanner(System.in);
        System.out.print("Masukkan tanggal 1 : ");
        tgl1 = in.nextInt();
        System.out.print("Masukkan bulan 1 : ");
        bulan1 = in.nextInt();
        System.out.print("Masukkan tahun 1 : ");
        thn1 = in.nextInt();
        System.out.print("Masukkan tanggal 2 : ");
        tgl2 = in.nextInt();
        System.out.print("Masukkan bulan 2 : ");
        bulan2 = in.nextInt();
        System.out.print("Masukkan tahun 2 : ");
        thn2 = in.nextInt();
        System.out.println("Tanggal ke 1 = " + tgl1 + "-" + bulan1 + "-" + thn1);
        System.out.println("Tanggal ke 2 = " + tgl2 + "-" + bulan2 + "-" + thn2);
        
        JumlahHari1 = (thn1*365) + (bulan1*30) + tgl1;
        JumlahHari2 = (thn2*365) + (bulan2*30) + tgl2;
        selisih = JumlahHari2 - JumlahHari1;
        System.out.println("Selisih hari dari kedua tanggal tersebut adalah " + selisih);
        
        thn3 = selisih / 365;
        Sisa = selisih % 365;
        bulan3 = Sisa / 30;
        tgl3 = Sisa % 30;
        System.out.println(thn3 + " Tahun " + bulan3 + " Bulan " + tgl3 + " Hari ");
    }   
}
