/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.IvaAlfiyanti.pertemuan.kesebelas;

import java.io.IOException;
import java.io.RandomAccessFile;

public class RandomArsip {

    public void setTulis(String file, int posisi, String record) {
        try {
            RandomAccessFile raf = new RandomAccessFile(file, "rw");
            raf.seek(posisi);
            raf.writeUTF(record);
            raf.close();
        } catch (IOException e) {
            System.err.println("Error : " + e.getMessage());

        }
    }

    public String getBaca(String file, int posisi) {
        String record = "";
        try {
            RandomAccessFile raf = new RandomAccessFile(file, "r");
            raf.seek(posisi);
            record = raf.readUTF();
            raf.close();
        } catch (IOException e) {
            System.err.println("Error : " + e.getMessage());
        }
        return record;
    }

    public static void main(String[] args) {
        String berkas = "D:\\Sequential FIle\\Iva.txt";
        String data = "NIM : 123 | Nama : IVA ";

        RandomArsip ra = new RandomArsip();
        ra.setTulis(berkas, 1, data);
        System.out.println("Tulis Berhasil");

        String output = ra.getBaca(berkas, 1);
        System.out.println("Baca Berhasil : " + output);
    }
}
