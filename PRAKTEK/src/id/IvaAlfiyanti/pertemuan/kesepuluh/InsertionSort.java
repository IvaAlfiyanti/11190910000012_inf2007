/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.IvaAlfiyanti.pertemuan.kesepuluh;

import java.util.Arrays;

/**
 *
 * @author Fujitsu
 */
public class InsertionSort {
    
    public static int[] getInsertionSort(int L[], int n) {
        int i, j, y;
        boolean ketemu;
        
        for (i = 1; i < n; i++) {
            y = L[i];
            j = i - 1;
            ketemu = false; 
            System.out.println("i : " + i + "j : " + L[j]);
            
            while ((j >= 0) && (!ketemu)) {
                if (y < L[j]) {
                    L[j+1] = L[j];
                    j = j - 1;
                }else {
                    ketemu = true;
                }
            }
            L[j+1] = y;
        }
        return L;
    }
    public static void main(String[] args) {
        int L[] = {25, 27, 10, 8, 76, 21};
        int n = 6;
        System.out.println("INSERTION SORT");
        
        System.out.println( Arrays.toString(L));
        InsertionSort app = new InsertionSort();
        app.getInsertionSort(L, n);
        System.out.println( Arrays.toString(L));
    }
}
