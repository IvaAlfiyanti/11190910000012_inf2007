package id.IvaAlfiyanti.pertemuan.kesepuluh;

import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author Fujitsu
 */
public class LarikMultidimensi {

    int[][] getLarikMultidimensi(int[][] L, int nBar, int nKol) {

        int i, j, temp, tambah = 0;
        int nTotal = nBar * nKol;
        int[] Array = new int[nTotal];

        for (i = 0; i < nBar; i++) {
            for (j = 0; j < nKol; j++) {
                Array[tambah] = L[i][j];
                tambah++;
            }
        }
        System.out.println("Nilai - Nilai Larik Penampung ");
        System.out.println(Arrays.toString(Array));

        for (i = 0; i < nTotal - 1; i++) {
            for (j = nTotal - 1; j > i; j--) {
                if (Array[j] < Array[j - 1]) {
                    temp = Array[j];
                    Array[j] = Array[j - 1];
                    Array[j - 1] = temp;
                }
            }
        }
        System.out.println("Nilai Larik Penampung yang Terurut ");
        System.out.println(Arrays.toString(Array));

        for (i = 0; i < nBar; i++) {
            for (j = 0; j < nKol; j++) {
                L[i][j] = Array[tambah];
                tambah++;
            }
        }

        return L;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        LarikMultidimensi sort = new LarikMultidimensi();
        int nBar, nKol;
        int i, j;
        System.out.println("Masukkan Jumlah Baris ");
        nBar = in.nextInt();
        System.out.println("Masukkan Jumlah Kolom ");
        nKol = in.nextInt();
        int[][] L = new int[nBar][nKol];
        for (i = 0; i < nBar; i++) {
            System.out.println("Masukkan Nilai Larik Baris ke " + (i + 1));
            for (j = 0; j < nKol; j++) {
                L[i][j] = in.nextInt();
            }
        }
        System.out.println("Larik Yang Belum Diurutkan ");
        System.out.println(Arrays.deepToString(L));
        sort.getLarikMultidimensi(L, nBar, nKol);
        System.out.println("Larik yang Sudah Diurutkan ");
        System.out.println(Arrays.deepToString(L));

    }
}
