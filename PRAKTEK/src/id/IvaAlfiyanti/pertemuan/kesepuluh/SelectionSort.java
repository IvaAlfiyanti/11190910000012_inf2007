/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.IvaAlfiyanti.pertemuan.kesepuluh;

import java.util.Arrays;

public class SelectionSort {
    
    public static int[] getSelectionSortMax(int L[], int n) {
        int i, j, imaks, temp;
        
        for (i = n - 1; i < 1; i--) {
            imaks = 0;
            for (j = 1; j < i; j++) {
                System.out.println("i : " + i + " j :" + L[j]);
                if (L[j] > L[imaks]) {
                    imaks = j;
            }
        }
        temp = L[i];
        L[i] = L[imaks];
        L[imaks] = temp;
        }
        return L;    
    }
    public static int[] getSelectionSortMin(int L[], int n ) {
        int i, j, imin,temp;
        
        for (i = 0; i < n - 1; i++) {
            imin = i;
            for (j = i + 1; j < n; j++) {
                System.out.println("i : " + i + " j :" + L[j]);
                if (L[j] < L[imin]) {
                    imin = j;
                }
            }
            temp = L[i];
            L[i] = L[imin];
            L[imin] = temp;
        }
        return L;
    }
    public static void main(String[] args) {
        int L[] = {25, 27, 10, 8, 76, 21};
        int n = 6;
        
        System.out.println("===SELECTION SORT===");
        SelectionSort app = new SelectionSort();
        System.out.println(Arrays.toString(L)); 
        app.getSelectionSortMax(L, n);
        System.out.println(Arrays.toString(L));
        //app.getSelectionSortMin(L, n);
        //System.out.println(Arrays.toString(L));
        
    }
}

