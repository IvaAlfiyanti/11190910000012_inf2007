/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.IvaAlfiyanti.pertemuan.kesepuluh;

import java.util.Arrays;

public class BubbleSort {
    
     public int[] getBubbleSort(int L[], int n) {
        int i, k;
        int temp;

        for (i = 0; i < n - 1; i++) {
            for (k = n - 1; k > i; k--) {
                System.out.println("i : " + i + ", k : " + L[k - 1]);
                if (L[k] < L[k - 1]) {
                    temp = L[k];
                    L[k] = L[k - 1];
                    L[k-1] = temp;
                }

            }
        }
        return L;
    }

    public static void main(String[] args) {

        int L[] = {25, 27, 10, 8, 76, 21};
        int n =6;
        System.out.println("===BUBBLE SORT===");

        System.out.println("Angka : " + Arrays.toString(L));
        BubbleSort app = new BubbleSort();
        app.getBubbleSort(L, n);
        System.out.println("Angka : " + Arrays.toString(L));
    }
}
