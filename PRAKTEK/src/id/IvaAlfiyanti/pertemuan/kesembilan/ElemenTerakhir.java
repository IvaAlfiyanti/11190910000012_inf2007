/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.IvaAlfiyanti.pertemuan.kesembilan;
import java.util.Scanner;

public class ElemenTerakhir {
    
    public int getIndeks(int[] L, int n, int x) {
        int i = n - 1;
        boolean ketemu = false;
        while ((i > -1) && (i <= n - 1) && (!ketemu)) {
            if (i <= n - 1) {
                System.out.println("Posisi ke : " + i + L[i]);
            }
            if (L[i] == x) {
                ketemu = true;
            }else {
                i = i - 1;
            }
            }
            if (ketemu) {
                return i;
            }else {
                return -1;
            }
        }
        
        public static void main(String[] args) {
            int i, x, n = 6;
            int[] L = new int[]{ 15, 17, 33, 76, 12, 25 };
            Scanner in = new Scanner(System.in);
            System.out.println("Masukan Nilai X : ");
            x = in.nextInt();
            ElemenTerakhir app = new ElemenTerakhir();
            System.out.println("Indeks : " + app.getIndeks(L, n, x));
        }
}