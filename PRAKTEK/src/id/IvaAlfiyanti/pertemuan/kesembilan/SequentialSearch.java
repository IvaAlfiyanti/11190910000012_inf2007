/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.IvaAlfiyanti.pertemuan.kesembilan;

import java.util.Scanner;

public class SequentialSearch {

    public boolean getSearchOutBoolean(int n, int L[], int x) {
        int i = 0;
        while ((i < n - 1) && (L[i] != x)) {
            if (i == 0) {
                System.out.println("posisi ke : " + i + " isinya " + L[i]);
            }
            i = i + 1;
            System.out.println("posisi ke : " + i + " isinya " + L[i]);
        }

        return L[i] == x;

    }

    public int getSearchOutIndeks(int n, int L[], int x) {
        int i = 0;
        while ((i < n - 1) && (L[i] != x)) {

            i = i + 1;
        }
        if (L[i] == x) {
            return i;

        } else {
            return -1;
        }
    }

    public Boolean getSearchInBoolean(int n, int L[], int x) {
        int i = 0;
        boolean ketemu = false;

        while ((i < n - 1) && (!ketemu)) {
            if (i == 0) {
                System.out.println("posisi ke : " + i + " isinya " + L[i]);
            }
            if (L[i] == x) {
                ketemu = true;
            } else {
                i = i + 1;
                System.out.println("posisi ke : " + i + " isinya " + L[i]);
            }
        }
        return ketemu;
    }

    public int getSearchInIndeks(int n, int L[], int x) {
        int i = 0;
        boolean ketemu;

        ketemu = false;
        while ((i < n - 1) && (!ketemu)) {
            if ( i == 0) {
                System.out.println("posisi ke : " + i + " isinya " + L[i]);
            }
            if (L[i] == x) {
                ketemu = true;
            } else {
                i = i + 1;
                System.out.println("posisi ke : " + i + " isinya " + L[i]);
            }
        }
        if (ketemu) {
            return i;
        } else {
            return -1;
        }
    }

    public int getSearchSentinel(int n, int L[], int x) {
        int i;
        int idx;

        L[n + 1] = x;
                i = 0;
        while (L[i] != x) {
        }
        if (x == L[n + 1]) {
            return idx = -1;
        }
            return idx = 1;
    }
    public static void main(String[] args) {
        int L[] = { 15, 17, 33, 76, 12, 25 };
        int i, x, n = 6;
        Scanner in = new Scanner(System.in);
        //int L[] = new int[6];
        SequentialSearch app = new SequentialSearch();
        System.out.println("Masukkan nilai X = ");
        x = in.nextInt();
        L [0] = 15;
        L [1] = 17;
        L [2] = 33;
        L [3] = 76;
        L [4] = 12;
        L [5] = 25;
        n = L.length - 2;
        
        System.out.println(" indeks : " + app.getSearchSentinel(n, L, x));
        //`int L[] = new int[]{ 15, 17, 33, 76, 12, 25 };
        //System.out.println(" indeks : " + app.getSearchOutBoolean(n, L, x));
        //System.out.println(" data ada di indeks yang ke : " + app.getSearchOutIndeks(n, L, x));
        //System.out.println(" data : " + app.getSearchInBoolean(n, L, x));
        //System.out.println(" data ada di indeks yang ke : " + app.getSearchInIndeks(n, L, x));
 
    }
}