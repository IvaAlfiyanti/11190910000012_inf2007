/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.IvaAlfiyanti.pertemuan.kesembilan;
import java.util.Scanner;
/**
 *
 * @author Fujitsu
 */
public class BinarySearch {
    
    private int i, j, k;
    private boolean ketemu;
    
    public int getBinarySearch(int n, int L[], int x) {
        i = 0;
        j = n;
        boolean ketemu = false;
        while ((!ketemu) && (i <= j)) {
            k = (i + j) / 2;
            if (L[k] == x) {
                ketemu = true;
            } else {
                if (L[k] < x) {
                    i = k + 1;
                } else {
                    j = k - 1;
                }
            }
        }
        if (ketemu) {
            return k;
        }else {
            return -1;
        }
    }
    public static void main(String[] args) {
        int L[] = { 15, 17, 33, 76, 12, 25 };
        int x, n = 6;
        
        BinarySearch app = new BinarySearch();
        Scanner in = new Scanner(System.in);
        System.out.println("Masukkan X : ");
        x = in.nextInt();
        
        System.out.println(" Indeks : " + app.getBinarySearch(n, L, x));
    }
}
    
