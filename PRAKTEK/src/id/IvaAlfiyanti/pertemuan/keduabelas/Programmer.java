/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.IvaAlfiyanti.pertemuan.keduabelas;

/**
 *
 * @author Fujitsu
 */
public class Programmer extends Pegawai {

    private int bonus;
    private int tunjangan;

    public Programmer(String nama, int gaji, int tunjangan) {
        super(nama, gaji);
        this.bonus = bonus;
        this.tunjangan = tunjangan;
    }

    public int infogaji() {
        return gaji + bonus;
    }

    public int infoBonus() {
        return gaji + bonus + tunjangan;
    }

}
