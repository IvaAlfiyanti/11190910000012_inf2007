/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.IvaAlfiyanti.pertemuan.keduabelas;

/**
 *
 * @author Fujitsu
 */
public class PenyimpananUang extends Tabungan {

    private double tingkatBunga;

    public PenyimpananUang(int saldo, double tingkatBunga) {
        super(saldo);
        this.tingkatBunga = tingkatBunga;
    }

    public double cekUang() {
        return saldo + (saldo * tingkatBunga);
    }

}
